#!/bin/bash
PULSEDEVICEID=$(cat /opt/vmware/iotc-agent/data/data/deviceIds.data)
/opt/vmware/iotc-agent/bin/DefaultClient stop-daemon
/opt/vmware/iotc-agent/bin/iotc-agent-cli unenroll --device-id=$PULSEDEVICEID
/opt/vmware/iotc-agent/uninstall.sh
rm -rf /home/pulseagent
