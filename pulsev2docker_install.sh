#!/bin/bash
docker pull trivediparth/pulsev2ubuntu:pulsev2ubuntu
DOCKERIMAGEID=$(docker images | grep pulsev2 | awk '{print $3}')
docker run -dit $DOCKERIMAGEID
DOCKERCONTAINERID=$(docker ps -a | grep $DOCKERIMAGEID | awk '{print $1}')
docker exec -it $DOCKERCONTAINERID "/bin/bash"
